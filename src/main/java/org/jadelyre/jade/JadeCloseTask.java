package org.jadelyre.jade;

import org.jadelyre.core.Channel;
import org.jadelyre.core.exception.JadeLyreException;
import org.jadelyre.core.task.Task;
import org.jadelyre.core.worker.Worker;

import java.io.IOException;
import java.nio.channels.AsynchronousSocketChannel;

public class JadeCloseTask extends Task<Channel> {

    public JadeCloseTask(Worker<Channel> worker) {
        super(worker);
    }

    @Override
    protected <T> void work(T t) throws JadeLyreException {
        Channel channel = (Channel) t;
        channel.getDecodeWorker().setTerminated(true);
        channel.getHandleWorker().setTerminated(true);
        channel.getSendWorker().setTerminated(true);
        AsynchronousSocketChannel asynchronousSocketChannel = channel.getAsynchronousSocketChannel();
        try {
            if (asynchronousSocketChannel.isOpen()) {
                asynchronousSocketChannel.shutdownInput().shutdownOutput().close();
            }
        } catch (IOException e) {
            throw new JadeLyreException(e);
        }
    }

}
