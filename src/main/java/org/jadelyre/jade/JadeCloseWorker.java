package org.jadelyre.jade;

import org.jadelyre.core.Channel;
import org.jadelyre.core.task.EntryQueue;
import org.jadelyre.core.worker.Worker;

import java.util.concurrent.ThreadPoolExecutor;

public class JadeCloseWorker extends Worker<Channel> {

    public JadeCloseWorker(ThreadPoolExecutor executor, EntryQueue<Channel> entryQueue) {
        super(executor, entryQueue, true);
    }

    @Override
    protected void execute() {
        executor.execute(new JadeCloseTask(this));
    }
}
