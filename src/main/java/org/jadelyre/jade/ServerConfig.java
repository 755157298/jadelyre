package org.jadelyre.jade;

import org.jadelyre.core.JadeLyreConfig;

public class ServerConfig extends JadeLyreConfig {

    private String host;
    private Integer port;
    private int defaultPort = 5021;
    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        if (port == null){
            return defaultPort;
        }
        return port;
    }
    public void setPort(Integer port) {
        this.port = port;
    }
}
