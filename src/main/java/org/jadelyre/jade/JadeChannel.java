package org.jadelyre.jade;

import org.jadelyre.core.AbstractChannel;
import org.jadelyre.core.NodeAddress;

import java.nio.channels.AsynchronousSocketChannel;

public class JadeChannel extends AbstractChannel {

    public JadeChannel(JadeServer jadeServer, AsynchronousSocketChannel asynchronousSocketChannel,
                       NodeAddress remoteNodeAddress) {
        super(jadeServer, asynchronousSocketChannel, remoteNodeAddress);
    }



}
