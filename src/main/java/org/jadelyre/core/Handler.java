package org.jadelyre.core;

import org.jadelyre.core.codec.Packet;
import org.jadelyre.lyre.LyreChannel;

public interface Handler {

    /**
     * 实际业务处理方法
     *
     * @param packet
     * @param channel
     */
    default void handle(Packet packet, Channel channel) {
        HeartbeatHandler heartbeatHandler = getHeartbeatHandler();
        if (heartbeatHandler != null) {
            if (channel instanceof LyreChannel) {
                channel.setLastReadTime(System.currentTimeMillis());//只有心跳检测的时候才开启
            }
            boolean b = heartbeatHandler.isHeartbeatPacket(packet);
            if (b) {
                heartbeatHandler.handle(packet, channel);
                return;
            }
        }
        doHandle(packet, channel);
    }

    void doHandle(Packet packet, Channel channel);

    HeartbeatHandler getHeartbeatHandler();
}
