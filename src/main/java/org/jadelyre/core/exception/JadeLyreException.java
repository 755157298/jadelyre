package org.jadelyre.core.exception;

/**
 * 不区分服务端和客户端的异常
 */
public class JadeLyreException extends RuntimeException {
    public JadeLyreException() {
        super();
    }
    public JadeLyreException(String message) {
        super(message);
    }

    public JadeLyreException(String message, Throwable cause) {
        super(message, cause);
    }

    public JadeLyreException(Throwable cause) {
        super(cause);
    }
}
