package org.jadelyre.core.exception;

/**
 * 服务端异常
 */
public class JadeException extends JadeLyreException {

    public JadeException() {
        super();
    }
    public JadeException(String message) {
        super(message);
    }

    public JadeException(String message, Throwable cause) {
        super(message, cause);
    }

    public JadeException(Throwable cause) {
        super(cause);
    }
}
