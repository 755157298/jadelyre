package org.jadelyre.core.exception;

/**
 * 客户端异常
 */
public class LyreException extends JadeLyreException {
    public LyreException() {
        super();
    }
    public LyreException(String message) {
        super(message);
    }

    public LyreException(String message, Throwable cause) {
        super(message, cause);
    }

    public LyreException(Throwable cause) {
        super(cause);
    }
}
