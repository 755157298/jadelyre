package org.jadelyre.core;

import org.jadelyre.core.exception.JadeLyreException;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPool {


    public static ThreadPoolExecutor createThreadPool(int poolSize) {
        if (poolSize <= 0) {
            throw new JadeLyreException("poolSize 参数必须大于0");
        }
        LinkedBlockingQueue<Runnable> linkedBlockingQueue = new LinkedBlockingQueue<>(1024);
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(poolSize, poolSize, 600, TimeUnit.SECONDS, linkedBlockingQueue);
        threadPoolExecutor.prestartCoreThread();
        return threadPoolExecutor;
    }

    public static ThreadPoolExecutor createScheduledThreadPool(int poolSize) {
        if (poolSize <= 0) {
            throw new JadeLyreException("poolSize 参数必须大于0");
        }
        ScheduledThreadPoolExecutor threadPoolExecutor = new ScheduledThreadPoolExecutor(poolSize);
        threadPoolExecutor.prestartCoreThread();
        threadPoolExecutor.setRemoveOnCancelPolicy(true);
        return threadPoolExecutor;
    }
}
