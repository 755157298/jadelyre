package org.jadelyre.core;

//import org.slf4j.Logger;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class Banner {

    public static String draw(JadeLyre jadeLyre){
        long start = System.currentTimeMillis();
        String baseStr = "|----------------------------------------------------------------------------|";
        StackTraceElement[] ses = Thread.currentThread().getStackTrace();
        StackTraceElement se = ses[ses.length - 1];
        List<String> infoList = new ArrayList<>();
        infoList.add("JadeLyre gitee address      | " + "https://www.mayun.com                        |");
        infoList.add("JadeLyre site address       | " + "https://www.guanwang.com                     |");
        infoList.add("JadeLyre version            | " + "1.0.0                                        |");

        infoList.add("---------------------------------------------------------------------------|");

        infoList.add("Started at                  | " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"))
                +"                          |");
        infoList.add("Listen on                   | " + "0.0.0.0:5021                                 |");
        infoList.add("Main Class                  | " + se.getClassName()+"                     |");

        try {
            RuntimeMXBean runtimeMxBean = ManagementFactory.getRuntimeMXBean();
            String runtimeName = runtimeMxBean.getName();
            String pid = runtimeName.split("@")[0];
            long startTime = runtimeMxBean.getStartTime();
            long startCost = System.currentTimeMillis() - startTime;
            infoList.add("Jvm start time              | " + startCost + " ms                                       |");
            infoList.add("Jade start time             | "
                    + (System.currentTimeMillis() - start) + " ms                                        |");
            infoList.add("Pid                         | " + pid+"                                         |");

        } catch (Exception e) {

        }
        String printStr = "\r\n"+baseStr+"\r\n";
        for (String string : infoList) {
            printStr += "| " + string  + "\r\n";
        }
        printStr += baseStr + "\r\n";
        return printStr;
    }

    public static void main(String[] args) {
        draw(null);
    }
}
