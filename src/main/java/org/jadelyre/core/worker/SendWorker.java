package org.jadelyre.core.worker;

import org.jadelyre.core.Channel;
import org.jadelyre.core.codec.Packet;
import org.jadelyre.core.task.EntryQueue;
import org.jadelyre.core.task.SendTask;

import java.util.concurrent.ThreadPoolExecutor;

public class SendWorker extends ChannelWorker<Packet> {

    public SendWorker(Channel channel, ThreadPoolExecutor executor, EntryQueue<Packet> entryQueue) {
        super(channel, executor, entryQueue);
    }

    @Override
    protected void execute() {
        executor.execute(new SendTask(channel, this));
    }
}
