package org.jadelyre.core.worker;

import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Schedule {

    private Type type;
    private Runnable command;
    private TimeUnit timeUnit;
    private long delay;
    private long initialDelay;
    private long period;
    private ScheduledFuture<?> future;

    public Schedule(Type type, Runnable command, TimeUnit timeUnit, long delay, long initialDelay, long period) {
        this.type = type;
        this.command = command;
        this.timeUnit = timeUnit;
        this.delay = delay;
        this.initialDelay = initialDelay;
        this.period = period;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Runnable getCommand() {
        return command;
    }

    public void setCommand(Runnable command) {
        this.command = command;
    }

    public TimeUnit getTimeUnit() {
        return timeUnit;
    }

    public void setTimeUnit(TimeUnit timeUnit) {
        this.timeUnit = timeUnit;
    }

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public long getInitialDelay() {
        return initialDelay;
    }

    public void setInitialDelay(long initialDelay) {
        this.initialDelay = initialDelay;
    }

    public long getPeriod() {
        return period;
    }

    public void setPeriod(long period) {
        this.period = period;
    }

    public ScheduledFuture<?> getFuture() {
        return future;
    }

    public void setFuture(ScheduledFuture<?> future) {
        this.future = future;
    }

    public enum Type {
        normal,
        schedule,
        schedule_fix_rate,
        schedule_fix_delay;
    }
}
