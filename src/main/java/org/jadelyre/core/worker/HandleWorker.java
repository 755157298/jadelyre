package org.jadelyre.core.worker;

import org.jadelyre.core.Channel;
import org.jadelyre.core.codec.Packet;
import org.jadelyre.core.task.EntryQueue;
import org.jadelyre.core.task.HandleTask;

import java.util.concurrent.ThreadPoolExecutor;


public class HandleWorker extends ChannelWorker<Packet> {

    public HandleWorker(Channel channel, ThreadPoolExecutor executor, EntryQueue<Packet> entryQueue) {
        super(channel, executor, entryQueue);
    }

    @Override
    protected void execute() {
        executor.execute(new HandleTask(channel, this));
    }
}
