package org.jadelyre.core.worker;

import org.jadelyre.core.Channel;
import org.jadelyre.core.task.DecodeTask;
import org.jadelyre.core.task.EntryQueue;

import java.nio.ByteBuffer;
import java.util.concurrent.ThreadPoolExecutor;

public class DecodeWorker extends ChannelWorker<ByteBuffer> {


    public DecodeWorker(Channel channel, ThreadPoolExecutor executor, EntryQueue<ByteBuffer> entryQueue) {
        super(channel, executor, entryQueue);
    }

    @Override
    protected void execute() {
        executor.execute(new DecodeTask(channel, this));
    }


}
