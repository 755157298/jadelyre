package org.jadelyre.core.worker;

import org.jadelyre.core.Channel;
import org.jadelyre.core.task.EntryQueue;

import java.util.concurrent.ThreadPoolExecutor;

public abstract class ChannelWorker<T> extends Worker<T> {

    protected Channel channel;

    public ChannelWorker(Channel channel,
                         ThreadPoolExecutor executor,
                         EntryQueue<T> entryQueue) {
        super(executor, entryQueue, true);
        this.channel = channel;
    }
}
