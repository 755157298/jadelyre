package org.jadelyre.core.task;

import org.jadelyre.core.Channel;
import org.jadelyre.core.worker.Worker;

public abstract class ChannelTask<T> extends Task<T> {

    protected final Channel channel;

    public ChannelTask(Channel channel, Worker<T> worker) {
        super(worker);
        this.channel = channel;
    }

}
