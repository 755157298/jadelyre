package org.jadelyre.core.task;

import org.jadelyre.core.Channel;
import org.jadelyre.core.Handler;
import org.jadelyre.core.codec.Packet;
import org.jadelyre.core.worker.Worker;

public class HandleTask extends ChannelTask<Packet> {
    private final Handler handler;

    public HandleTask(Channel channel, Worker<Packet> worker) {
        super(channel, worker);
//        executor = channel.getJadeLyre().getWorkerFactory().getHandleWorker();
        handler = channel.getJadeLyre().getHandler();
    }

//    public Handler getHandler() {
//        return handler;
//    }


    @Override
    protected <T> void work(T t) {
        handler.handle((Packet) t, super.channel);
    }
}
