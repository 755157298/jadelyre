package org.jadelyre.core.task;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class EntryQueue<T> {

    private ConcurrentLinkedQueue<T> entryQueue = new ConcurrentLinkedQueue<>();
    private AtomicInteger count = new AtomicInteger(0);

    public EntryQueue addEntry(T t) {
        boolean add = entryQueue.add(t);
        if (add) {
            count.getAndIncrement();
        }
        return this;
    }

    public T takeEntry() {
        T t = entryQueue.poll();
        if (t != null && count.get() > 0) {
            count.getAndDecrement();
        }
        return t;
    }

    public int getCount() {
        return count.get();
    }
}
