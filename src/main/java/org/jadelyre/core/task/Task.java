package org.jadelyre.core.task;


import org.jadelyre.core.exception.JadeLyreException;
import org.jadelyre.core.worker.Worker;

public abstract class Task<T> implements Runnable {

    private Worker<T> worker;

    public Task(Worker<T> worker) {
        this.worker = worker;
    }

    @Override
    public void run() {
        try {
            for (; ; ) {
                T t = worker.getEntryQueue().takeEntry();
                if (null == t) {
                    break;
                }
                work(t);
            }
        } catch (Exception e) {
            throw new JadeLyreException(e);
        } finally {
            worker.getTaskCount().getAndDecrement();
        }

    }
    protected abstract <T> void work(T t) throws Exception;

}
