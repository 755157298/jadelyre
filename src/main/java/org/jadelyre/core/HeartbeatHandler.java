package org.jadelyre.core;

import org.jadelyre.core.codec.Packet;

/**
 * 心跳检查逻辑是客户端定期检查从服务端接收到
 * 的数据包(包括数据包和心跳包)，发送心跳包是
 * 从客户端发向服务端
 */
public interface HeartbeatHandler {
    /**
     * 判断数据包是否是心跳包,
     * 服务端需要以此方法判断是否是心跳包
     *
     * @param packet
     * @return
     */
    boolean isHeartbeatPacket(Packet packet);

    /**
     * 处理心跳包
     * 服务端需要对客户端发来的心跳包进行处理，返回心跳响应
     * @param packet
     * @param channel
     */
    void handle(Packet packet, Channel channel);

    /**
     * 创建心跳包
     * 客户端需要发送的心跳包
     * @return
     */
    Packet createHeartbeatPacket();

    /**
     * 心跳超时业务处理方法
     * 客户端如果检查服务端心跳超时需要进行的处理
     * @param channel
     */
    void handleHeartbeatTimeout(Channel channel);
}
