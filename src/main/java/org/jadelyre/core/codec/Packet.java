package org.jadelyre.core.codec;

import java.io.Serializable;
import java.nio.ByteOrder;

/**
 * 传输包
 */
public interface Packet extends Serializable {
    ByteOrder BYTE_ORDER = ByteOrder.BIG_ENDIAN;

    /**
     * 获取数据体
     */
    byte[] getBody();

}
