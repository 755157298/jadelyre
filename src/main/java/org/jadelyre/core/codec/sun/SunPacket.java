package org.jadelyre.core.codec.sun;

import org.jadelyre.core.codec.Packet;

/**
 * 默认数据包实现
 */
public class SunPacket implements Packet {
    private static final long serialVersionUID = -3920583706958121305L;
    public static final String CHARSET = "utf-8";
    /**头
     */
    private SunHeader header;
    /**
     * 体
     */
    private byte[] body;

    public SunHeader getHeader() {
        return header;
    }

    @Override
    public byte[] getBody() {
        return body;
    }

    public PacketMark getPacketMark() {
        byte packetMark = header.getPacketMark();
        return PacketMark.match(packetMark);
    }


    private SunPacket(SunHeader header,byte[] body) {
        this.header = header;
        this.body = body;
    }

    /**
     * 编码时构造数据包
     * @param body
     * @return
     */
    public static SunPacket encodeBuild(byte[] body){
        if (body == null){
            return null;
        }
        return encodeBuild(body,PacketMark.DATA_PACKET);
    }

    /**
     * 编码时构造数据包
     * @param body
     * @param packetMark
     * @return
     */
    public static SunPacket encodeBuild(byte[] body, PacketMark packetMark){
        if (body == null){
            return null;
        }
        if (packetMark == null){
            packetMark = PacketMark.DATA_PACKET;
        }
        SunHeader sunHeader = new SunHeader(body.length, packetMark.getB());
        return new SunPacket(sunHeader,body);
    }


    /**
     * 解码时构造数据包
     * @param header
     * @param body
     * @return
     */
    public static SunPacket decodeBuild(SunHeader header,byte[] body){
        return new SunPacket(header,body);
    }


}
