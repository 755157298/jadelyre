package org.jadelyre.core.codec.sun;

import java.nio.ByteBuffer;

/**
 * <p>
 *  默认包协议消息头
 * </p>
 *
 * @author jlh
 * @create 2019/10/22
 */
public class SunHeader {
    //消息标识+消息体长度
    private static final int HEADER_LENGTH = 4 + 1;
    /**
     * 消息体长度
     */
    private int bodyLength;
    /**
     * 消息标识：0 普通包; 1心跳包
     */
    private byte packetMark;


    public SunHeader(int bodyLength, byte packetMark) {
        this.bodyLength = bodyLength;
        this.packetMark = packetMark;
    }

    public int getBodyLength() {
        return bodyLength;
    }

    public byte getPacketMark() {
        return packetMark;
    }


    //解码取出头信息
    public static SunHeader getHeader(ByteBuffer byteBuffer){
        byte packetMark = byteBuffer.get();
        int bodyLength = byteBuffer.getInt();
        return new SunHeader(bodyLength,packetMark);
    }

    public static int getHeaderLength() {
        return HEADER_LENGTH;
    }
}
