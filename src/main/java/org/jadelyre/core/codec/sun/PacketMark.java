package org.jadelyre.core.codec.sun;

/**
 *  包标识
 * @author jlh
 */
public enum PacketMark {
    /**
     * 数据包
     */
    DATA_PACKET((byte) 0),

    /**
     *心跳包
     */
    HEARTBEAT_PACKET((byte) 1);

    private byte b;

    PacketMark(byte b) {
        this.b = b;
    }

    public byte getB() {
        return b;
    }

    public static PacketMark match(byte b) {
        for (PacketMark packetMark : PacketMark.values()) {
            if (packetMark.b == b){
                return packetMark;
            }
        }
        return DATA_PACKET;
    }
}
