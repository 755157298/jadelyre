package org.jadelyre.core;

import org.jadelyre.core.codec.Codec;
import org.jadelyre.core.worker.Schedule;
import org.jadelyre.core.worker.Worker;
import org.jadelyre.core.worker.WorkerFactory;

public interface JadeLyre {

    JadeLyreConfig getJadeLyreConfig();

    WorkerFactory getWorkerFactory();

    Handler getHandler();

    Worker<Channel> getCloseWorker();

    Worker<Schedule> getScheduledWorker();

    Codec getCodec();

    void stop();

    boolean isStarted();
}
