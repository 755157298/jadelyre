package org.jadelyre.core;

import org.jadelyre.core.codec.Packet;
import org.jadelyre.core.worker.Worker;

import java.nio.ByteBuffer;
import java.nio.channels.AsynchronousSocketChannel;

public interface Channel {

    JadeLyre getJadeLyre();

    NodeAddress getRemoteNodeAddress();

    Worker<ByteBuffer> getDecodeWorker();

    Worker<Packet> getHandleWorker();

    Worker<Packet> getSendWorker();

    void close();

    ReadCompletionHandler getReadCompletionHandler();

    WriteCompletionHandler getWriteCompletionHandler();

    AsynchronousSocketChannel getAsynchronousSocketChannel();

    /**
     * 最近一次收到回应的时间
     *
     * @return
     */
    long getLastReadTime();

    /**
     * 设置最近一次回应时间
     *
     * @param pongTime
     */
    void setLastReadTime(long pongTime);
}
