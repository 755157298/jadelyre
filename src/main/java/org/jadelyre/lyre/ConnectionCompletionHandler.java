package org.jadelyre.lyre;

import org.jadelyre.core.NodeAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.channels.AsynchronousSocketChannel;
import java.nio.channels.CompletionHandler;
import java.util.concurrent.CountDownLatch;

public class ConnectionCompletionHandler implements CompletionHandler<Void, LyreChannel> {
    private static final Logger logger = LoggerFactory.getLogger(ConnectionCompletionHandler.class);
    private CountDownLatch countDownLatch;

    @Override
    public void completed(Void result, LyreChannel lyreChannel) {
        LyreClient lyreClient = (LyreClient) lyreChannel.getJadeLyre();
        lyreClient.setStarted(true);
        lyreClient.setFirstConnect(false);
        AsynchronousSocketChannel asynchronousSocketChannel = lyreChannel.getAsynchronousSocketChannel();
        asynchronousSocketChannel.read(lyreChannel.getReadCompletionHandler().getReadByteBuffer(), lyreChannel, lyreChannel.getReadCompletionHandler());
        countDownLatch.countDown();
    }

    @Override
    public void failed(Throwable exc, LyreChannel lyreChannel) {
        if (logger.isErrorEnabled()) {
            NodeAddress remoteNodeAddress = lyreChannel.getRemoteNodeAddress();
            logger.error("connect remote server fail：{}:{}", remoteNodeAddress.getIp(), remoteNodeAddress.getPort());
        }
        lyreChannel.close();
    }

    public void setCountDownLatch(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }
}
