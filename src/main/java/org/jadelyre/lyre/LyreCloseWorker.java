package org.jadelyre.lyre;

import org.jadelyre.core.Channel;
import org.jadelyre.core.task.EntryQueue;
import org.jadelyre.core.worker.Worker;

import java.util.concurrent.ThreadPoolExecutor;

public class LyreCloseWorker extends Worker<Channel> {

    public LyreCloseWorker(ThreadPoolExecutor executor, EntryQueue<Channel> entryQueue) {
        super(executor, entryQueue, true);
    }

    @Override
    protected void execute() {
        executor.execute(new LyreCloseTask(this));
    }
}
