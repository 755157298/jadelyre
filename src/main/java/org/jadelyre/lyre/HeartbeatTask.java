package org.jadelyre.lyre;

import org.jadelyre.core.Channel;
import org.jadelyre.core.HeartbeatHandler;

public class HeartbeatTask implements Runnable {
    private HeartbeatHandler handler;
    private Channel channel;
    private int timeout;

    public HeartbeatTask(HeartbeatHandler handler,
                         Channel channel,
                         int timeout) {
        this.handler = handler;
        this.channel = channel;
        this.timeout = timeout;
    }

    @Override
    public void run() {
        long currentTime = System.currentTimeMillis();
        if ((currentTime - channel.getLastReadTime()) / 1000 > timeout) {
            handler.handleHeartbeatTimeout(channel);
        }
        channel.getSendWorker().work(handler.createHeartbeatPacket());
    }
}
