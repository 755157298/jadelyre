package org.jadelyre.lyre;

/**
 * 重连配置
 */
public class ReconnectConfig {
    /**
     * 是否启用重连机制，默认不重连
     */
    private boolean enable;
    /**
     * 重连间隔周期，单位秒
     */
    private int period;
    /**
     * 重连次数，为0或负数代表一直重连
     */
    private int count;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }


}
