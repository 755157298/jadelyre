package org.jadelyre.lyre;

public class HeartbeatConfig {
    /**
     * 是否开启心跳检查
     */
    private boolean enable;
    /**
     * 心跳超时时间，单位秒
     */
    private int timeout;

    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

}
