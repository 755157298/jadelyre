package org.jadelyre.lyre;

import org.jadelyre.core.AbstractChannel;
import org.jadelyre.core.NodeAddress;

import java.nio.channels.AsynchronousSocketChannel;

public class LyreChannel extends AbstractChannel {

    private ConnectionCompletionHandler connectionCompletionHandler;

    public LyreChannel(LyreClient lyreClient, AsynchronousSocketChannel asynchronousSocketChannel,
                       NodeAddress remoteNodeAddress) {
        super(lyreClient, asynchronousSocketChannel, remoteNodeAddress);
        this.connectionCompletionHandler = new ConnectionCompletionHandler();
    }

    public ConnectionCompletionHandler getConnectionCompletionHandler() {
        return connectionCompletionHandler;
    }
}
