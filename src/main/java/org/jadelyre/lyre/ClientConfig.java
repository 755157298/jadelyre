package org.jadelyre.lyre;

import org.jadelyre.core.JadeLyreConfig;

/**
 * 客户端配置
 */
public class ClientConfig extends JadeLyreConfig {
    /**
     * 服务端host，一般为ip
     */
    private String serverHost;
    /**
     * 服务端端口，一般默认服务端端口为：0521
     */
    private Integer serverPort;
    /**
     * 客户端绑定host，一般不用设置
     */
    private String bindHost;
    /**
     * 客户端绑定端口，一般不用设置
     */
    private Integer bindPort;
    /**
     * 连接超时时间，单位秒，默认5s
     */
    private int connectTimeout = 5;
    /**
     * 重连配置
     */
    private ReconnectConfig reconnectConfig = new ReconnectConfig();
    /**
     * 心跳配置
     */
    private HeartbeatConfig heartbeatConfig;

    public String getServerHost() {
        return serverHost;
    }

    public void setServerHost(String serverHost) {
        this.serverHost = serverHost;
    }

    public Integer getServerPort() {
        return serverPort;
    }

    public void setServerPort(Integer serverPort) {
        this.serverPort = serverPort;
    }

    public String getBindHost() {
        return bindHost;
    }

    public void setBindHost(String bindHost) {
        this.bindHost = bindHost;
    }

    public Integer getBindPort() {
        return bindPort;
    }

    public void setBindPort(Integer bindPort) {
        this.bindPort = bindPort;
    }

    public ReconnectConfig getReconnectConfig() {
        return reconnectConfig;
    }

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public void setConnectTimeout(int connectTimeout) {
        this.connectTimeout = connectTimeout;
    }

    public void setReconnectConfig(ReconnectConfig reconnectConfig) {
        this.reconnectConfig = reconnectConfig;
    }

    public HeartbeatConfig getHeartbeatConfig() {
        return heartbeatConfig;
    }

    public void setHeartbeatConfig(HeartbeatConfig heartbeatConfig) {
        this.heartbeatConfig = heartbeatConfig;
    }
}
