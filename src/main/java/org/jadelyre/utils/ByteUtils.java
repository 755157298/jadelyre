package org.jadelyre.utils;

/**
 * <p>
 *
 * </p>
 *
 * @author jlh
 * @create 2019/10/22
 */
public class ByteUtils {

    public static byte[] intToByteArray(int i) {
        byte[] result = new byte[4];
        // 由高位到低位
        result[0] = (byte) ((i >> 24) & 0xFF);
        result[1] = (byte) ((i >> 16) & 0xFF);
        result[2] = (byte) ((i >> 8) & 0xFF);
        result[3] = (byte) (i & 0xFF);
        return result;
    }
}
