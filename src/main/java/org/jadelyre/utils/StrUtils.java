package org.jadelyre.utils;

public class StrUtils {

    public static boolean isBlank(String str){
        return (str == null || "".equals(str));
    }
}
