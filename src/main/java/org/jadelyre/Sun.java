package org.jadelyre;

import org.jadelyre.core.Channel;
import org.jadelyre.core.codec.sun.SunPacket;
import org.jadelyre.core.exception.JadeLyreException;

public class Sun {

    public static void send(SunPacket packet, Channel channel) {
        if (packet == null) {
            throw new JadeLyreException("packet must be not null");
        }
        if (channel == null) {
            throw new JadeLyreException("channel must be not null,check use channel that has been closed");
        }
        channel.getSendWorker().work(packet);
    }
}
