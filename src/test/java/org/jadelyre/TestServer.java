package org.jadelyre;

import org.jadelyre.core.Channel;
import org.jadelyre.core.Handler;
import org.jadelyre.core.HeartbeatHandler;
import org.jadelyre.core.codec.Packet;
import org.jadelyre.core.codec.sun.PacketMark;
import org.jadelyre.core.codec.sun.SunPacket;
import org.jadelyre.jade.JadeServer;
import org.jadelyre.jade.ServerConfig;

/**
 * @author jlh
 */
public class TestServer {

    //serverHandler 需要自己实现根据业务分别为
    // 服务端和客户端实现org.jadelyre.core.Handler接口
    public static void main(String[] args) {
        ServerConfig serverConfig = new ServerConfig();
        serverConfig.setSystemPoolSize(3);
        serverConfig.setSharedPoolSize(3);
        serverConfig.setScheduledPoolSize(2);
        ServerHandler serverHandler = new ServerHandler();
        JadeServer jadeServer = new JadeServer(serverConfig,serverHandler);
        jadeServer.start();
    }

    static class ServerHandler implements Handler {

        @Override
        public void doHandle(Packet packet, Channel channel) {
            System.out.println(new String(packet.getBody()));
            //故意不返回造成客户端心跳检查超时
            SunPacket sunPacket = SunPacket.encodeBuild("hello,server response".getBytes());
            Sun.send(sunPacket,channel);
        }

        @Override
        public HeartbeatHandler getHeartbeatHandler() {
            return new DefaultHeartbeatHandler();
        }
    }

    static class DefaultHeartbeatHandler implements HeartbeatHandler {

        @Override
        public boolean isHeartbeatPacket(Packet packet) {
            SunPacket sunPacket = (SunPacket) packet;
            PacketMark packetMark = sunPacket.getPacketMark();
            return packetMark == PacketMark.HEARTBEAT_PACKET;
        }

        @Override
        public void handle(Packet packet, Channel channel) {
            System.out.println("客户端来心跳了");
        }

        @Override
        public Packet createHeartbeatPacket() {
            SunPacket sunPacket = SunPacket.encodeBuild("ping".getBytes(), PacketMark.HEARTBEAT_PACKET);
            return sunPacket;
        }

        @Override
        public void handleHeartbeatTimeout(Channel channel) {
            System.out.println("服务端超时了啊");
        }
    }
}
