package org.jadelyre;

import org.jadelyre.core.Channel;
import org.jadelyre.core.Handler;
import org.jadelyre.core.HeartbeatHandler;
import org.jadelyre.core.codec.Packet;
import org.jadelyre.core.codec.sun.PacketMark;
import org.jadelyre.core.codec.sun.SunPacket;
import org.jadelyre.lyre.ClientConfig;
import org.jadelyre.lyre.HeartbeatConfig;
import org.jadelyre.lyre.LyreClient;

import java.io.IOException;

/**
 * @author jlh
 */
public class TestClient {
    public static void main(String[] args) throws IOException {
        ClientHandler clientHandler = new ClientHandler();
        HeartbeatConfig heartbeatConfig = new HeartbeatConfig();
        heartbeatConfig.setEnable(true);
        heartbeatConfig.setTimeout(5);
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.setServerHost("127.0.0.1");
        clientConfig.setServerPort(5021);
        clientConfig.setSystemPoolSize(3);
        clientConfig.setSharedPoolSize(3);
        clientConfig.setScheduledPoolSize(2);
        clientConfig.getReconnectConfig().setEnable(true);
        clientConfig.setHeartbeatConfig(heartbeatConfig);
        LyreClient LyreClient = new LyreClient(clientConfig, clientHandler);
        LyreClient.connect();
        SunPacket sunPacket = SunPacket.encodeBuild("hello,client request".getBytes());
        for (; ; ) {
            Sun.send(sunPacket, LyreClient.getChannel());
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    static class ClientHandler implements Handler {


        @Override
        public void doHandle(Packet packet, Channel channel) {
//            SunPacket sunPacket = SunPacket.encodeBuild("hello,client request".getBytes());
//            Sun.send(sunPacket,channel);
        }

        @Override
        public HeartbeatHandler getHeartbeatHandler() {
            return new DefaultHeartbeatHandler();
        }
    }


    static class DefaultHeartbeatHandler implements HeartbeatHandler {

        @Override
        public boolean isHeartbeatPacket(Packet packet) {
            SunPacket sunPacket = (SunPacket) packet;
            PacketMark packetMark = sunPacket.getPacketMark();
            return packetMark == PacketMark.HEARTBEAT_PACKET;
        }

        @Override
        public void handle(Packet packet, Channel channel) {
            System.out.println("客户端来心跳了");
        }

        @Override
        public Packet createHeartbeatPacket() {
            SunPacket sunPacket = SunPacket.encodeBuild("ping".getBytes(), PacketMark.HEARTBEAT_PACKET);
            return sunPacket;
        }

        @Override
        public void handleHeartbeatTimeout(Channel channel) {
            System.out.println("服务端超时了啊");
        }
    }
}
