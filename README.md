# jadelyre

#### 项目介绍
网络通讯底层组件,纯依赖jdk编写的AIO,不依赖任何其他依赖,与netty同级,
可以在此之上封装各种服务,目前本人基于此编写了一个http服务:
[koon](https://gitee.com/755157298/koon)

#### 软件架构

1. 目前总共分4个包,分别为:
- core: tcp包编解码协议,异常处理,编解码线程任务,解析的完整包传递给业务
处理线程任务,发送数据包线程任务,核心的抽象接口封装等
- jade: 服务端逻辑模块
- lyre: 客户端逻辑模块
- utils: 工具类
- Sun类,方便的方法

2. + 核心四个类处理连接和读写数据
     + `AcceptCompletionHandler`
     + `ReadCompletionHandler`
     + `WriteCompletionHandler`
     + `ConnectionCompletionHandler`
   + 核心的默认tcp包编解包类
     + `SunEnde`
#### 安装教程

1. 克隆项目源码,编译安装：`mvn install`，在项目中引入此依赖。

2. 根据自己的业务实现`org.jadelyre.core.Handler`接口，可以分别为客户端和服务端实现此接口

3. 服务端运行：
```java
import org.jadelyre.core.Channel;
import org.jadelyre.core.Handler;
import org.jadelyre.core.codec.Packet;
import org.jadelyre.core.codec.sun.SunPacket;
import org.jadelyre.jade.JadeServer;
import org.jadelyre.jade.ServerConfig;

/**
 * @author jlh
 */
public class TestServer {

    //serverHandler 需要自己实现根据业务分别为
    // 服务端和客户端实现org.jadelyre.core.Handler接口
    public static void main(String[] args) {
        ServerConfig serverConfig = new ServerConfig();
        serverConfig.setSystemPoolSize(3);
        serverConfig.setSharedPoolSize(3);
        serverConfig.setScheduledPoolSize(2);
        ServerHandler serverHandler = new ServerHandler();
        JadeServer jadeServer = new JadeServer(serverConfig,serverHandler);
        jadeServer.start();
    }
    static class ServerHandler implements Handler {
    
            @Override
            public void doHandle(Packet packet, Channel channel) {
                System.out.println(new String(packet.getBody()));
                //故意不返回造成客户端心跳检查超时
    //            SunPacket sunPacket = SunPacket.encodeBuild("hello,server response".getBytes());
    //            Sun.send(sunPacket,channel);
            }
    
            @Override
            public HeartbeatHandler getHeartbeatHandler() {
                return new DefaultHeartbeatHandler();
            }
        }
    
        static class DefaultHeartbeatHandler implements HeartbeatHandler {
    
            @Override
            public boolean isHeartbeatPacket(Packet packet) {
                SunPacket sunPacket = (SunPacket) packet;
                PacketMark packetMark = sunPacket.getPacketMark();
                return packetMark == PacketMark.HEARTBEAT_PACKET;
            }
    
            @Override
            public void handle(Packet packet, Channel channel) {
                System.out.println("客户端来心跳了");
            }
    
            @Override
            public Packet createHeartbeatPacket() {
                SunPacket sunPacket = SunPacket.encodeBuild("ping".getBytes(), PacketMark.HEARTBEAT_PACKET);
                return sunPacket;
            }
    
            @Override
            public void handleHeartbeatTimeout(Channel channel) {
                System.out.println("服务端超时了啊");
            }
        }
}
   
```
4. 客户端运行：
```java
import org.jadelyre.core.Channel;
import org.jadelyre.core.Handler;
import org.jadelyre.core.codec.Packet;
import org.jadelyre.core.codec.sun.SunPacket;
import org.jadelyre.lyre.ClientConfig;
import org.jadelyre.lyre.LyreClient;

/**
 * @author jlh
 */
public class TestClient {
    public static void main(String[] args) {
        ClientHandler clientHandler = new ClientHandler();
        HeartbeatConfig heartbeatConfig = new HeartbeatConfig();
        heartbeatConfig.setEnable(true);//开启心跳检查
        heartbeatConfig.setTimeout(5);//心跳间隔时间5秒，同时超时时间也是5秒
        ClientConfig clientConfig = new ClientConfig();
        clientConfig.setServerHost("127.0.0.1");
        clientConfig.setServerPort(5021);
        clientConfig.setSystemPoolSize(3);
        clientConfig.setSharedPoolSize(3);
        clientConfig.setScheduledPoolSize(2);
        clientConfig.getReconnectConfig().setEnable(true);//开启断线自动重连
        clientConfig.setHeartbeatConfig(heartbeatConfig);//心跳配置
        LyreClient LyreClient = new LyreClient(clientConfig,clientHandler);
        Channel channel = LyreClient.connect();
        SunPacket sunPacket = SunPacket.encodeBuild("hello,client request".getBytes());
        Sun.send(sunPacket, channel);
    }
    static class ClientHandler implements Handler {
    
    
            @Override
            public void doHandle(Packet packet, Channel channel) {
    //            SunPacket sunPacket = SunPacket.encodeBuild("hello,client request".getBytes());
    //            Sun.send(sunPacket,channel);
            }
    
            @Override
            public HeartbeatHandler getHeartbeatHandler() {
                return new DefaultHeartbeatHandler();
            }
        }
    
    
        static class DefaultHeartbeatHandler implements HeartbeatHandler {
    
            @Override
            public boolean isHeartbeatPacket(Packet packet) {
                SunPacket sunPacket = (SunPacket) packet;
                PacketMark packetMark = sunPacket.getPacketMark();
                return packetMark == PacketMark.HEARTBEAT_PACKET;
            }
    
            @Override
            public void handle(Packet packet, Channel channel) {
                System.out.println("客户端来心跳了");
            }
    
            @Override
            public Packet createHeartbeatPacket() {
                SunPacket sunPacket = SunPacket.encodeBuild("ping".getBytes(), PacketMark.HEARTBEAT_PACKET);
                return sunPacket;
            }
    
            @Override
            public void handleHeartbeatTimeout(Channel channel) {
                System.out.println("服务端超时了啊");
            }
        }
}
   
```


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)